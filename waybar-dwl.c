#define _GNU_SOURCE
#include <errno.h>
#include <poll.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/signalfd.h>
#include <unistd.h>

#define LENGHT(A) (sizeof(A) / sizeof(A[0]))

static const char *const tags[] = {
	"󰈹", "", "", "", "󰈙", "󰈟", "󰈫", "󰈣", "", "",
};

#define DWL_OUTPUT_FILENAME "/run/user/1000/dwl-wayland-0" /* For simplicity we do not parse env vars (yet?) */
#define MAX_TITLE_LENGTH    90                   /* Adjust according to available space in YOUR waybar - prevent right-side overflow */
#define PANGO_TAG_DEFAULT   "<span foreground='#989710'>" /* Pango span style for 'default' tags */
#define PANGO_TAG_ACTIVE    "<span overline='single' overline_color='#fe8019'>" /* Pango span style for 'active' tags */
#define PANGO_TAG_SELECTED  "<span foreground='#458588'>" /* Pango span style for 'selected' tags */
#define PANGO_TAG_URGENT    "<span background='#fb4934'>" /* Pango span style for 'urgent' tags */
#define PANGO_LAYOUT        "<span foreground='#fe8019'>" /* Pango span style for 'layout' character */
#define PANGO_TITLE         "<span foreground='#458588'>" /* Pango span style for 'title' monitor */
#define PANGO_INACTIVE      "<span foreground='#928374'>" /* Pango span style for elements on an INACTIVE monitor */
#define PANGO_END           "</span>"

struct dwl_output {
	char *monitor;
	bool should_print;
	char *title;
	char *app_id;
	bool fullscreen;
	bool floating;
	bool selmon;
	uint32_t tags[4];
	char *layout;
};

static void
die(char *fmt, ...)
{
	va_list ap;
	
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[0] && fmt[strlen(fmt) - 1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	} else {
		fputc('\n', stderr);
	}
	
	exit(1);
}

static void
warn(char *fmt, ...)
{
	va_list ap;
	
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[0] && fmt[strlen(fmt) - 1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	} else {
		fputc('\n', stderr);
	}
}

static void
print_dwl_output(const struct dwl_output *output)
{
	if (!output->should_print)
		return;

	const uint32_t activetags = output->tags[0];
	const uint32_t selectedtags = output->tags[1];
	const uint32_t urgenttags = output->tags[3];

	char const* pango_tag_default = output->selmon ? PANGO_TAG_DEFAULT : PANGO_INACTIVE;
	char const* pango_layout = output->selmon ? PANGO_LAYOUT : PANGO_INACTIVE;
	char const* pango_title = output->selmon ? PANGO_TITLE : PANGO_INACTIVE;

	printf("{");
	printf("\"text\": \"");
	for (size_t i = 0; i < LENGHT(tags); i++) {
		const uint32_t mask = 1 << i;
		char *tag_text = strdup(tags[i]), *tmp = NULL;
		if ((activetags & mask)) {
			tmp = strdup(tag_text);
			free(tag_text);
			if (asprintf(&tag_text, PANGO_TAG_ACTIVE "%s" PANGO_END, tmp) < 0)
				die("asprintf:");
			free(tmp);
		}
		if ((urgenttags & mask)) {
			tmp = strdup(tag_text);
			free(tag_text);
			if (asprintf(&tag_text, PANGO_TAG_URGENT "%s" PANGO_END, tmp) < 0)
				die("asprintf:");
			free(tmp);
		}
		if ((selectedtags & mask)) {
			tmp = strdup(tag_text);
			free(tag_text);
			if (asprintf(&tag_text, PANGO_TAG_SELECTED "%s" PANGO_END, tmp) < 0)
				die("asprintf:");
			free(tmp);
		} else {
			tmp = strdup(tag_text);
			free(tag_text);
			if (asprintf(&tag_text, "%s" "%s" PANGO_END, pango_tag_default, tmp) < 0)
				die("asprintf");
			free(tmp);
		}

		printf("%s  ", tag_text);
		free(tag_text);
	}

	printf("%s" "%s " PANGO_END, pango_layout, output->layout);
	printf("%s" "%s" PANGO_END, pango_title, output->title);

	printf("\"");
	printf("}\n");
	fflush(stdout);
}

static void
process_line(char buffer[1024], struct dwl_output *output)
{
	char *saveptr;
	char *buf = strtok_r(buffer, " ", &saveptr);

	output->should_print = 0;

	if (strcmp(buf, output->monitor))
		return;
	buf = strtok_r(NULL, " ", &saveptr);
	if (!strcmp(buf, "title")) {
		free(output->title);
		output->title = strndup(saveptr, MAX_TITLE_LENGTH - 1);
		output->title[strlen(output->title) - 1] = '\0';
		for (size_t i = 0; i < strlen(output->title); i++) {
			if (output->title[i] == '"')
				output->title[i] = '\'';
			if (output->title[i] == '&')
				output->title[i] = '+';
		}
		// fprintf(stderr, "title: %s\n", output->title);
	} else if (!strcmp(buf, "appid")) {
		free(output->app_id);
		output->app_id = strdup(saveptr);
		output->app_id[strlen(output->app_id) - 1] = '\0';
		// fprintf(stderr, "app_id: %s\n", output->app_id);
	} else if (!strcmp(buf, "fullscreen")) {
		output->fullscreen = strtoul(saveptr, NULL, 10);
		// fprintf(stderr, "fullscreen: %d\n", output->fullscreen);
	} else if (!strcmp(buf, "floating")) {
		output->floating = strtoul(saveptr, NULL, 10);
		// fprintf(stderr, "floating: %d\n", output->floating);
	} else if (!strcmp(buf, "selmon")) {
		output->selmon = strtoul(saveptr, NULL, 10);
		// fprintf(stderr, "selmon: %d\n", output->selmon);
	} else if (!strcmp(buf, "tags")) {
		if (sscanf(saveptr,  "%"SCNu32" %"SCNu32" %"SCNu32" %"SCNu32,
					&output->tags[0], &output->tags[1], &output->tags[2], &output->tags[3])
				< (int)LENGHT(output->tags))
			die("Failed to parse dwl tags");
		// fprintf(stderr, "tags: %d, %d, %d, %d\n", output->tags[0], output->tags[1], output->tags[2], output->tags[3]);
	} else if (!strcmp(buf, "layout")) {
		free(output->layout);
		output->layout = strdup(saveptr);
		output->layout[strlen(output->layout) - 1] = '\0';
		output->should_print = true;
		// fprintf(stderr, "layout: %s\n", output->layout);
	}
}

static int
process_file(FILE *file, struct dwl_output *output)
{
	static char buffer[1024];

	while (fgets(buffer, LENGHT(buffer), file)) {
		process_line(buffer, output);

		print_dwl_output(output);
	}

	fseek(file, 0, SEEK_END);

	return ferror(file) == 0;
}

int
main(int argc, char *argv[])
{
	bool sucess = true;
	if (argc < 2)
		die("Usage: %s <monitor>", argv[0]);

	int inotify_fd = inotify_init();
	if (inotify_fd < 0)
		die("Unable to create inotify_fd:");

	int inotify_wd = inotify_add_watch(inotify_fd, DWL_OUTPUT_FILENAME, IN_MODIFY);
	if (inotify_wd < 0) {
		warn("Unable to add watch to inotify_fd:");
		sucess = false;
		goto close_inotify_fd;
	}

	FILE *file = fopen(DWL_OUTPUT_FILENAME, "r");
	if (!file) {
		warn("Unable to open file:");
		sucess = false;
		goto rm_inotify_watch;
	}

	/* Only read the last 35 lines */
	int limit_lines = 35;
	
	fseek(file, 0, SEEK_END);
	long position = ftell(file);

    while (position > 0) {
        /* Cannot go less than position 0 */
        if (fseek(file, --position, SEEK_SET) == EINVAL)
            break;

        if (fgetc(file) == '\n')
            if (limit_lines-- == 0)
                break;
    }

	sigset_t smask;
	sigemptyset(&smask);
	sigaddset(&smask, SIGINT);
	sigaddset(&smask, SIGTERM);
	int sfd = signalfd(-1, &smask, SFD_CLOEXEC);
	if (sfd < 0) {
		warn("signalfd:");
		sucess = false;
		goto close_file;
	}

	if (sigprocmask(SIG_SETMASK, &smask, NULL) < 0) {
		warn("sigprocmask:");
		sucess = false;
		goto close_sfd;
	}

	/* Process the output */
	struct dwl_output output = {0};
	output.monitor = strdup(argv[1]);
	if (!(sucess = process_file(file, &output)))
		goto end;

	while (true) {
		struct pollfd fds[] = {
			{.fd = inotify_fd, .events = POLLIN},
			{.fd = sfd, .events = POLLIN},
		};

		if (poll(fds, LENGHT(fds), -1) < 0) {
			if (errno == EINTR)
				continue;

			sucess = false;
			warn("Unable to poll:");
			break;
		}

		if (fds[1].revents & POLLIN)
			break;

		if (!(fds[0].revents & POLLIN)) {
			warn("expected POLLIN revent");
			sucess = false;
			break;
		}

		static char buffer[1024];
		ssize_t lenght = read(inotify_fd, buffer, LENGHT(buffer));

		if (lenght == 0)
			break;

		if (lenght < 0) {
			if (errno == EAGAIN)
				continue;
			warn("unable to read %s", DWL_OUTPUT_FILENAME);
		}

		// fprintf(stderr, "Processing file\n");
		if (!(sucess = process_file(file, &output)))
			break;
	}

end:
	free(output.title);
	free(output.app_id);
	free(output.layout);
	free(output.monitor);
close_sfd:
	close(sfd);
close_file:
	fclose(file);
rm_inotify_watch:
	inotify_rm_watch(inotify_fd, inotify_wd);
close_inotify_fd:
	close(inotify_fd);

	return !sucess;
}
